//
//  CollectionViewCell.swift
//  superpads2
//
//  Created by Carmine on 09/12/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import UIKit
//Collection View Class with the ImageView Outlet
class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var padImageView: UIImageView!
    
}
