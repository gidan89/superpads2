//
//  ViewController.swift
//  superpads2
//
//  Created by Carmine on 09/12/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import UIKit
import AVFoundation



class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var avplayers: [AVAudioPlayer] = []
    var now: TimeInterval = 0.0
    var restart: TimeInterval = 0.0
    @IBOutlet weak var collectionView: UICollectionView!
     
    
    @IBOutlet weak var lcdScreen: UIImageView!
    
    @IBOutlet weak var counter: UILabel!
    
    let lcdpng: UIImage = UIImage(named: "mpcscreen")!
    let heart: UIImage = UIImage(named: "heart")!
    
   
   
 
    //Content of the Cell passed by an array
    var padSound : [padsSounds] = [
        padsSounds(immagine: UIImage(named: "lead1")!,sounds: "ModernRhodes", immaginon: UIImage(named:"lead1on")!),
        padsSounds(immagine: UIImage(named: "lead2")!,sounds: "Fuzzy",immaginon: UIImage(named: "lead2on")!),
        padsSounds(immagine: UIImage(named: "lead3")!,sounds: "SynthBrass",immaginon: UIImage(named: "lead3on")!),
        padsSounds(immagine: UIImage(named: "pad1")!,sounds: "Pad",immaginon: UIImage(named: "pad1on")!),
        padsSounds(immagine: UIImage(named: "pad2")!,sounds: "RhodesPiano",immaginon: UIImage(named: "pad2on")!),
        padsSounds(immagine: UIImage(named: "pad3")!,sounds: "ChoirPad",immaginon: UIImage(named: "pad3on")!),
        padsSounds(immagine: UIImage(named: "bass1")!,sounds: "ArcadeBass",immaginon: UIImage(named: "bass1on")!),
        padsSounds(immagine: UIImage(named: "bass2")!,sounds: "FretlessBass",immaginon: UIImage(named: "bass2on")!),
        padsSounds(immagine: UIImage(named: "bass3")!,sounds: "PulseLegatoBass",immaginon: UIImage(named: "bass3on")!),
        padsSounds(immagine: UIImage(named: "kick")!,sounds: "Kick909",immaginon: UIImage(named: "kickon")!),
        padsSounds(immagine: UIImage(named: "hihat")!,sounds: "Maracas",immaginon: UIImage(named: "hihaton")!),
        padsSounds(immagine: UIImage(named: "snare")!,sounds: "Snare",immaginon: UIImage(named: "snareon")!),
    ]
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        self.collectionView.allowsMultipleSelection = true
        counter.text = ""
         
       
    }
    
   /*-------------------------------------------------------------------------*/
    //Lock the view to portrait mode
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
        lcdScreen.image = lcdpng
        lcdScreen.frame = CGRect(x: 18, y: 125, width: 339, height: 120)
        counter.frame = CGRect(x: 60 ,y: 125, width:300, height:100)
        counter.textColor = UIColor.black
        counter.font = UIFont(name: "Futura-Bold", size: CGFloat(22))
        
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

       
        AppUtility.lockOrientation(.all)
    }
   /*-------------------------------------------------------------------------*/
    
    //Cell number of items Count
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return padSound.count
     }
     
    
    //Generate Cell with the value of the arrays and also populate the arrays of Player
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        cell.padImageView.image = padSound[indexPath.item].immagine
        
      // Player
        let fileName = padSound[(indexPath).row].sounds
        let url = URL(fileURLWithPath:Bundle.main.path(forResource:fileName,ofType:"wav")!)
        var avplayer = AVAudioPlayer()
        do {
           try avplayer = AVAudioPlayer(contentsOf: url)
            avplayer.numberOfLoops = 90
            avplayer.volume = 1
            avplayer.prepareToPlay()
            now = avplayer.deviceCurrentTime
            avplayers.append(avplayer)
        } catch let error {
            print(error)
        }
        return cell
    }
    
    
    //Change image whern the cellview is selected and play associate to the array of player: Select
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       let cell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
        cell.padImageView.image = padSound[indexPath.item].immaginon
        counter.text = "-  \(padSound[indexPath.item].sounds)"
        
         
        
        avplayers[indexPath.row].play(atTime: now)
        
        print (now)
      
       // print("playing \(indexPath.row)")

    }
    
    //Stop the sound and return to default image: Deselect
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
       let cell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
        cell.padImageView.image = padSound[indexPath.item].immagine
        avplayers[indexPath.row].stop()
        avplayers[indexPath.row].currentTime = restart
        counter.text = " "
    }
    
   
   
}



